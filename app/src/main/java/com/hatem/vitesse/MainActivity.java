package com.hatem.vitesse;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Created by Hatem on 20/03/2020.
 */

public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
